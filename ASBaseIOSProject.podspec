
Pod::Spec.new do |s|

  s.name         = "ASBaseIOSProject"
  s.version      = "0.9.5"
  s.summary      = "ASBaseIOSProject is a collection of codes used as base of ios project"

  s.description  = <<-DESC
  ASBaseIOSProject is a collection of codes which can be used in ios application development. This include some classes to help developer in developing ios application
                   DESC

  s.homepage     = "http://appslon.com"

  s.license      = { :type => "Apache License, Version 2.0", :file => "LICENSE" }

  s.author             = { "jefrydagucci" => "jefrydagucci@gmail.com" }
  s.social_media_url   = "http://twitter.com/jefrydagucci"

  s.platform     = :ios
  s.ios.deployment_target = "7.0"

  s.source       = { :git => "https://bitbucket.org/jefrydagucci/asbaseiosproject.git", :tag => "v#{s.version}" }

  s.source_files  = "Classes", "Classes/**/*.{h,m}"
  s.resources = ["images/*.png", "Classes/**/*.{xib}"]

  s.requires_arc = true

end
