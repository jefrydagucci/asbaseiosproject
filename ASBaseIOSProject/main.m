//
//  main.m
//  ASBaseIOSProject
//
//  Created by Jefry Da Gucci on 2/12/15.
//  Copyright (c) 2015 Appslon http://appslon.com. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ASAppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([ASAppDelegate class]));
    }
}
