//
//  ViewController.m
//  ASExampleProject
//
//  Created by Jefry Da Gucci on 2/25/15.
//  Copyright (c) 2015 Appslon http://appslon.com. All rights reserved.
//
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//  http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.
//

#import "ViewController.h"

#import <ASBaseIOSProject/ASBaseIOSProject.h>
#import "Entity.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [[ASDatabaseManager sharedInstance]setDBName:@"ASExample"];
    [self insertObject];
    [self fetchObject];
    [self deleteObject];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - ASDatabaseManager example
- (void)fetchObject{
    [[ASDatabaseManager sharedInstance]fetchObjectForEntity:NSStringFromClass([Entity class]) fetchRequestBlock:^(NSFetchRequest *fetchRequest) {
        
        [fetchRequest setReturnsObjectsAsFaults:NO];
//        [fetchRequest setPredicate:@"your predicate"];
        
    } completion:^(NSArray *results,
                   NSManagedObjectContext *context,
                   NSError *error) {
        ASLog(results);
    }];
}

- (void)insertObject{
    [[ASDatabaseManager sharedInstance]insertObjectToEntity:NSStringFromClass([Entity class]) insertionBlock:^(NSManagedObject *object) {
        
        [(Entity *)object setItem1:@"item 10"];
        [(Entity *)object setItem2:@"item 2"];
        
    } completion:^(NSError *error) {
        if(error){
            ASLog(error.localizedDescription);
        }
    }];
}

- (void)deleteObject{
    [[ASDatabaseManager sharedInstance] deleteObjectForEntity:NSStringFromClass([Entity class]) fetchRequestBlock:^(NSFetchRequest *fetchRequest) {
        [fetchRequest setPredicate:[NSPredicate predicateWithFormat:@"item2 == %@", @"item 2"]];
    } completion:^(NSError *error) {
        ASLog(error.userInfo);
    }];
    
    [[ASDatabaseManager sharedInstance] deleteAllObjectsForEntity:NSStringFromClass([Entity class]) completion:^(NSError *error) {
        ASLog(error.userInfo);
    }];
}

@end
