//
//  ASRemoteNotificationManager.m
//  ASBaseIOSProject
//
//  Created by Jefry Da Gucci on 2/18/15.
//  Copyright (c) 2015 Appslon http://appslon.com. All rights reserved.
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//  http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.
//

#import "ASRemoteNotificationManager.h"

@implementation ASRemoteNotificationManager

+ (void)registerRemoteNotificationWithUserNotificationTypes:(UIUserNotificationType)types{
    UIApplication *application = [UIApplication sharedApplication];
    if ([application respondsToSelector:@selector(registerUserNotificationSettings:)]) {
        UIUserNotificationSettings *settings = [UIUserNotificationSettings settingsForTypes:types categories:nil];
        [application registerUserNotificationSettings:settings];
    }
    else{
        NSLog(@"doesn't respond to %@", NSStringFromSelector(@selector(registerUserNotificationSettings:)));
    }
}

+ (void)registerRemoteNotificationWithRemoteNotificationTypes:(UIRemoteNotificationType)types{
    UIApplication *application = [UIApplication sharedApplication];
    [application registerForRemoteNotificationTypes:types];
}

+ (NSString *)tokenForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken{
    const unsigned *tokenBytes = [deviceToken bytes];
    NSString *hexToken = [NSString stringWithFormat:@"%08x%08x%08x%08x%08x%08x%08x%08x",
                          ntohl(tokenBytes[0]), ntohl(tokenBytes[1]), ntohl(tokenBytes[2]),
                          ntohl(tokenBytes[3]), ntohl(tokenBytes[4]), ntohl(tokenBytes[5]),
                          ntohl(tokenBytes[6]), ntohl(tokenBytes[7])];
    return hexToken;
}



@end
