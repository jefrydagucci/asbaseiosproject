//
//  ASDatabaseManager.h
//  ASBaseIOSProject
//
//  Created by Jefry Da Gucci on 2/24/15.
//  Copyright (c) 2015 Appslon http://appslon.com. All rights reserved.
//
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//  http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.
//

#ifdef __OBJC__
#import <CoreData/CoreData.h>
#endif

#import "ASSharedObject.h"

/**
 `ASDatabaseManager` is a class to manage database using core data. By using this class, it is easier to manage your data saved on database.
 */
@interface ASDatabaseManager : ASSharedObject

/**
 This is your database name. If you don't set this, so the default will be your application name
 @default Your application name (CFBundleName)
 */
@property (nonatomic, strong) NSString *DBName;

/**
 The managed object model for your application database.
 */
@property (nonatomic, strong) NSManagedObjectModel *managedObjectModel;

/**
 Persistent coordinator for your application database.
 */
@property (nonatomic, strong) NSPersistentStoreCoordinator *persistentStoreCoordinator;

/**
 Managed object context for your application database.
 */
@property (nonatomic, strong) NSManagedObjectContext *managedObjectContext;

/**
 @return persistent store URL
 */
- (NSURL *)persistenStoreURL;

#pragma mark - Core Data fetch
/**
 Fetchs objects for chosen entity.
 @param entityName The chosen entity
 @param fetchRequestBlock The block to be executed which has no return value and takes one argument: the fetch request object that you can custom before fetch request execution.
 @param completion The block to be executed which has no return value and takes three arguments: the results of entity fetch request, the context and the error occured on fetch request execution.
 */
- (void)fetchObjectForEntity:(NSString *)entityName
           fetchRequestBlock:(void(^)(NSFetchRequest *fetchRequest))fetchRequestBlock
                  completion:(void(^)(NSArray *results,
                                      NSManagedObjectContext *context,
                                      NSError *error))completion;

/**
 Insert object to chosen entity.
 @param entityName The chosen entity
 @param insertionBlock The block to be executed which has no return value and takes one argument: the managed object of the chosen entity.
 @param completion The block to be executed which has no return value and takes one argument: the the error occured on fetch request execution.
 */
#pragma mark - Core Data insert
- (void)insertObjectToEntity:(NSString *)entityName
              insertionBlock:(void(^)(NSManagedObject *object))insertionBlock
                  completion:(void(^)(NSError *error))completion;

#pragma mark - Core Data delete
/**
 Delete object in chosen entity.
 @param entityName The chosen entity
 @param fetchRequestBlock The block to be executed which has no return value and takes one argument: the fetch request for this entity. You may set predicate to limit the object which you want to delete.
 @param completion The block to be executed which has no return value and takes one argument: the the error occured on fetch request execution.
 */
- (void)deleteObjectForEntity:(NSString *)entityName
            fetchRequestBlock:(void(^)(NSFetchRequest *fetchRequest))fetchRequestBlock
                   completion:(void(^)(NSError *error))completion;

/**
 Delete all objects in chosen entity.
 @param entityName The chosen entity
 @param completion The block to be executed which has no return value and takes one argument: the the error occured on fetch request execution.
 */
- (void)deleteAllObjectsForEntity:(NSString *)entityName
                       completion:(void(^)(NSError *error))completion;

/**
 Clear data stored in database
 */
- (void)clearDatabase;

@end
