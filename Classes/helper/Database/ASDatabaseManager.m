//
//  ASDatabaseManager.m
//  ASBaseIOSProject
//
//  Created by Jefry Da Gucci on 2/24/15.
//  Copyright (c) 2015 Appslon http://appslon.com. All rights reserved.
//
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//  http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.
//

#import "ASDocumentManager.h"
#import "ASLogManager.h"

#import "ASDatabaseManager.h"

@interface ASDatabaseManager ()

@property (nonatomic, assign) BOOL shouldChangeModel;
@end

@implementation ASDatabaseManager

- (void)setDBName:(NSString *)DBName{
    _DBName = DBName;
    self.shouldChangeModel = YES;
    [self managedObjectContext];
    self.shouldChangeModel = NO;
}

- (NSManagedObjectModel *)managedObjectModel{
    
    if (_managedObjectModel != nil && !self.shouldChangeModel) {
        return _managedObjectModel;
    }
    
    NSURL *modelURL = [[NSBundle mainBundle] URLForResource:[self databaseName] withExtension:@"momd"];
    
    _managedObjectModel = [[NSManagedObjectModel alloc] initWithContentsOfURL:modelURL];
    
    return _managedObjectModel;
}

- (NSURL *)persistenStoreURL{
    return [[self dataBasePath] URLByAppendingPathComponent:[NSString stringWithFormat:@"%@.sqlite", [self databaseName]]];
}

- (NSPersistentStoreCoordinator *)persistentStoreCoordinator{
    
    if (_persistentStoreCoordinator != nil && !self.shouldChangeModel) {
        return _persistentStoreCoordinator;
    }
    
    _persistentStoreCoordinator = [[NSPersistentStoreCoordinator alloc] initWithManagedObjectModel:[self managedObjectModel]];
    
    [self createDatabaseDirectoryIfNotExist];
    NSURL *storeURL = [self persistenStoreURL];
    
    NSError *error = nil;
    NSString *failureReason = @"There was an error creating or loading the application's saved data.";
    
    NSDictionary *option = [NSDictionary dictionaryWithObjectsAndKeys:
                            [NSNumber numberWithBool:YES], NSMigratePersistentStoresAutomaticallyOption,
                            [NSNumber numberWithBool:YES], NSInferMappingModelAutomaticallyOption, nil];
    
    if (![_persistentStoreCoordinator addPersistentStoreWithType:NSSQLiteStoreType configuration:nil URL:storeURL options:option error:&error]) {
        
        NSMutableDictionary *dict = [NSMutableDictionary dictionary];
        dict[NSLocalizedDescriptionKey] = @"Failed to initialize the application's saved data";
        dict[NSLocalizedFailureReasonErrorKey] = failureReason;
        dict[NSUnderlyingErrorKey] = error;
        error = [NSError errorWithDomain:@"YOUR_ERROR_DOMAIN" code:9999 userInfo:dict];
        
        NSString *errorString = [NSString stringWithFormat:@"Unresolved error %@, %@", error, [error userInfo]];
        ASLog(errorString);
    }
    
    return _persistentStoreCoordinator;
}


- (NSManagedObjectContext *)managedObjectContext{
    
    if (_managedObjectContext != nil && !self.shouldChangeModel) {
        return _managedObjectContext;
    }
    
    NSPersistentStoreCoordinator *coordinator = [self persistentStoreCoordinator];
    if (!coordinator) {
        return nil;
    }
    _managedObjectContext = [[NSManagedObjectContext alloc] init];
    [_managedObjectContext setPersistentStoreCoordinator:coordinator];
    return _managedObjectContext;
}

- (NSString *)databaseName{
    NSString *appName = [[NSBundle mainBundle] objectForInfoDictionaryKey:@"CFBundleName"];
    return !self.DBName?appName:self.DBName;
}

- (NSString *)databaseDirectoryName{
    return @"Database";
}

- (NSURL *)dataBasePath{
    return [[ASDocumentManager applicationDocumentsDirectoryURL] URLByAppendingPathComponent:[self databaseDirectoryName] isDirectory:YES];
}

/**
 Create database directory if it doesn't exist
 */
- (void)createDatabaseDirectoryIfNotExist{
    NSFileManager *defaultManager = [NSFileManager defaultManager];
    BOOL isDir = YES;
    NSString *dir = [[[self dataBasePath].absoluteString stringByDeletingPathExtension] stringByReplacingOccurrencesOfString:@"file:" withString:@""];
    
    if(![defaultManager fileExistsAtPath:dir isDirectory:&isDir]){
        if(![defaultManager createDirectoryAtURL:[self dataBasePath] withIntermediateDirectories:NO attributes:nil error:nil]){
            ASLog(@"Failed to create central data dir");
            return;
        }
        ASLog(@"Create Database directory");
    }
}

#pragma mark - Core Data fetch
- (void)fetchObjectForEntity:(NSString *)entityName
           fetchRequestBlock:(void(^)(NSFetchRequest *fetchRequest))fetchRequestBlock
                  completion:(void(^)(NSArray *results,
                                      NSManagedObjectContext *context,
                                      NSError *error))completion{
    
    NSFetchRequest *fetchRequest= [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription entityForName:entityName inManagedObjectContext:self.managedObjectContext];
    [fetchRequest setEntity:entity];
    
    !fetchRequestBlock?:fetchRequestBlock(fetchRequest);
    
    NSError *error = nil;
    NSArray *fetchedObjects = [self.managedObjectContext executeFetchRequest:fetchRequest error:&error];
    !completion?:completion(fetchedObjects, self.managedObjectContext, error);
}

#pragma mark - Core Data insert
- (void)insertObjectToEntity:(NSString *)entityName
              insertionBlock:(void(^)(NSManagedObject *object))insertionBlock
                  completion:(void(^)(NSError *error))completion{
    
    NSEntityDescription *entity = [NSEntityDescription entityForName:entityName inManagedObjectContext:self.managedObjectContext];
    NSManagedObject *object = [[NSManagedObject alloc]initWithEntity:entity insertIntoManagedObjectContext:self.managedObjectContext];
    
    !insertionBlock?:insertionBlock(object);
    
    NSError *error = nil;
    NSString *successMessage = [NSString stringWithFormat:@"Data saved to %@", entityName];
    [self.managedObjectContext save:&error]?ASLog(successMessage):ASLog(error.userInfo);
    !completion?:completion(error);
}

#pragma mark - Core Data delete
- (void)deleteObjectForEntity:(NSString *)entityName
            fetchRequestBlock:(void(^)(NSFetchRequest *fetchRequest))fetchRequestBlock
                   completion:(void(^)(NSError *error))completion{
    
    [self fetchObjectForEntity:entityName fetchRequestBlock:^(NSFetchRequest *fetchRequest) {
        
        !fetchRequestBlock?:fetchRequestBlock(fetchRequest);
        
    } completion:^(NSArray *results, NSManagedObjectContext *context, NSError *error) {
        
        for(NSManagedObject *object in results){
            [context deleteObject:object];
        }
        NSError *err;
        [context save:&err];
        !completion?:completion(err);
    }];
}

- (void)deleteAllObjectsForEntity:(NSString *)entityName
                       completion:(void(^)(NSError *error))completion{
    
    [self fetchObjectForEntity:entityName
             fetchRequestBlock:nil
                    completion:^(NSArray *results, NSManagedObjectContext *context, NSError *error) {
                        
                        for(NSManagedObject *object in results){
                            [context deleteObject:object];
                        }
                        NSError *err;
                        [context save:&err];
                        !completion?:completion(err);
                    }];
}

- (void)clearDatabase{
    NSArray *entities = [[self managedObjectModel] entities];
    @try {
        for(NSEntityDescription *entity in entities){
            [self deleteAllObjectsForEntity:[entity managedObjectClassName] completion:^(NSError *error) {
                if(error){
                    NSString *errMsg = [NSString stringWithFormat:@"Error : %@", error.userInfo];
                    ASLog(errMsg);
                }
            }];
        }
    }
    @catch (NSException *exception) {
        NSString *errMsg = [NSString stringWithFormat:@"Catch Exception : %@", exception.userInfo];
        ASLog(errMsg);
    }
    @finally {
        ASLog(@"Finish Clear Database");
    }
}

@end
