//
//  UITableViewCell+ASHelper.h
//  ASBaseIOSProject
//
//  Created by Jefry Da Gucci on 2/20/15.
//  Copyright (c) 2015 Appslon http://appslon.com. All rights reserved.
//
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//  http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.
//

#import <UIKit/UIKit.h>

@interface UITableViewCell (ASHelper)

#pragma mark - nib registration
/**
 Method user to register nib for chosen UITableView.
 @param tableView UITableView which will use registered nib
 @param bundle registered nib bundle
 @param reuseIdentifier identifier for registered UITableViewCell
 */
+ (void)registerNibForTable:(UITableView *)tableView bundle:(NSBundle *)bundle reuseIdentifier:(NSString *)reuseIdentifier;

/**
 Method user to register nib in bundle for chosen UITableView. The reuse identifier will be the class name of nib.
 @param tableView UITableView which will use registered nib
 @param bundle registered nib bundle
 */
+ (void)registerNibForTable:(UITableView *)tableView bundle:(NSBundle *)bundle;

/**
 Method user to register class for chosen UITableView
 @param tableView UITableView which will use registered class
 @param bundle registered class bundle
 @param reuseIdentifier identifier for registered UITableViewCell
 */
#pragma mark - class registration
+ (void)registerClassForTable:(UITableView *)tableView bundle:(NSBundle *)bundle reuseIdentifier:(NSString *)reuseIdentifier;

/**
 Method user to register class in bundle for chosen UITableView. The reuse identifier will be the class name.
 @param tableView UITableView which will use registered class
 @param bundle registered class bundle
 */
+ (void)registerClassForTable:(UITableView *)tableView bundle:(NSBundle *)bundle;

@end
