//
//  UITableViewCell+ASHelper.m
//  ASBaseIOSProject
//
//  Created by Jefry Da Gucci on 2/20/15.
//  Copyright (c) 2015 Appslon http://appslon.com. All rights reserved.
//
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//  http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.
//

#import "UITableViewCell+ASHelper.h"

@implementation UITableViewCell (ASHelper)

#pragma mark - nib registration
+ (void)registerNibForTable:(UITableView *)tableView bundle:(NSBundle *)bundle reuseIdentifier:(NSString *)reuseIdentifier{
    [tableView registerNib:[UINib nibWithNibName:NSStringFromClass([self class]) bundle:bundle] forCellReuseIdentifier:reuseIdentifier];
}

+ (void)registerNibForTable:(UITableView *)tableView bundle:(NSBundle *)bundle{
    NSString *className = NSStringFromClass([self class]);
    [self registerNibForTable:tableView bundle:bundle reuseIdentifier:className];
}

#pragma mark - class registration
+ (void)registerClassForTable:(UITableView *)tableView bundle:(NSBundle *)bundle reuseIdentifier:(NSString *)reuseIdentifier{
    [tableView registerClass:[self class] forCellReuseIdentifier:reuseIdentifier];
}

+ (void)registerClassForTable:(UITableView *)tableView bundle:(NSBundle *)bundle{
    NSString *className = NSStringFromClass([self class]);
    [self registerClassForTable:tableView bundle:bundle reuseIdentifier:className];
}

@end
