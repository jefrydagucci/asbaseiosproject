//
//  UITableView+ASHelper.h
//  ASBaseIOSProject
//
//  Created by Jefry Da Gucci on 2/20/15.
//  Copyright (c) 2015 Appslon http://appslon.com. All rights reserved.
//
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//  http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.
//

#import <UIKit/UIKit.h>

@interface UITableView (ASHelper)

#pragma mark - UITableView reload
/**
 Method used to reload data with completion
 @param completion completion block after UITableView reload
 */
- (void)reloadDataWithCompletion:(void(^)(void))completion;

/**
 Method used to reload data for chosen index paths with completion and animation
 @param indexPaths Chosen index paths will be reloaded on UITableView
 @param animation Animation option for UITableView row
 @param completion completion block after UITableView reload
 */
- (void)reloadRowsAtIndexPaths:(NSArray *)indexPaths withRowAnimation:(UITableViewRowAnimation)animation completion:(void(^)(void))completion;

/**
 Method used to reload data for chosen index path with completion and animation
 @param indexPath Chosen index path will be reloaded on UITableView
 @param animation Animation option for UITableView row
 @param completion completion block after UITableView reload
 */
- (void)reloadRowsAtIndexPath:(NSIndexPath *)indexPath withRowAnimation:(UITableViewRowAnimation)animation completion:(void(^)(void))completion;

/**
 Method used to reload data for chosen section with completion and animation
 @param sections Chosen sections will be reloaded on UITableView
 @param animation Animation option for UITableView row
 @param completion completion block after UITableView reload
 */
- (void)reloadSections:(NSIndexSet *)sections withRowAnimation:(UITableViewRowAnimation)animation completion:(void(^)(void))completion;

/**
 Method used to reload data for chosen section with completion and animation
 @param section Chosen section will be reloaded on UITableView
 @param animation Animation option for UITableView row
 @param completion completion block after UITableView reload
 */
- (void)reloadSection:(NSInteger)section withRowAnimation:(UITableViewRowAnimation)animation completion:(void(^)(void))completion;

@end
