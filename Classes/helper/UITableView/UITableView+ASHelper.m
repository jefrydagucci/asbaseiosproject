//
//  UITableView+ASHelper.m
//  ASBaseIOSProject
//
//  Created by Jefry Da Gucci on 2/20/15.
//  Copyright (c) 2015 Appslon http://appslon.com. All rights reserved.
//
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//  http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.
//

#import "UITableView+ASHelper.h"

@implementation UITableView (ASHelper)

#pragma mark - UITableView reload

- (void)reloadDataWithCompletion:(void(^)(void))completion{
    [self reloadData];
    !completion?:completion();
}

- (void)reloadRowsAtIndexPaths:(NSArray *)indexPaths withRowAnimation:(UITableViewRowAnimation)animation completion:(void(^)(void))completion{
    [self reloadRowsAtIndexPaths:indexPaths withRowAnimation:animation];
    !completion?:completion();
}

- (void)reloadRowsAtIndexPath:(NSIndexPath *)indexPath withRowAnimation:(UITableViewRowAnimation)animation completion:(void(^)(void))completion{
    [self reloadRowsAtIndexPaths:[NSArray arrayWithObjects:indexPath, nil] withRowAnimation:animation];
    !completion?:completion();
}

- (void)reloadSections:(NSIndexSet *)sections withRowAnimation:(UITableViewRowAnimation)animation completion:(void(^)(void))completion{
    [self reloadSections:sections withRowAnimation:animation];
    !completion?:completion();
}

- (void)reloadSection:(NSInteger)section withRowAnimation:(UITableViewRowAnimation)animation completion:(void(^)(void))completion{
    
    [self reloadSections:[NSIndexSet indexSetWithIndex:section]
        withRowAnimation:animation
              completion:completion];
}

@end
