//
//  ASRemoteNotificationManager.h
//  ASBaseIOSProject
//
//  Created by Jefry Da Gucci on 2/18/15.
//  Copyright (c) 2015 Appslon http://appslon.com. All rights reserved.
//
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//  http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.
//

#import "ASSharedObject.h"
#import <UIKit/UIKit.h>

@interface ASRemoteNotificationManager : ASSharedObject

/**
 Register remote notification for chosen user notification types
 @param types user notification type
 */
+ (void)registerRemoteNotificationWithUserNotificationTypes:(UIUserNotificationType)types NS_AVAILABLE_IOS(8_0);

/**
 Register remote notification for chosen remote notification types
 @param types remote notification type
 */
+ (void)registerRemoteNotificationWithRemoteNotificationTypes:(UIRemoteNotificationType)types NS_DEPRECATED_IOS(3_0, 8_0, "Please use [ASRemoteNotificationManager registerRemoteNotificationWithUserNotificationTypes:(UIRemoteNotificationType)types] instead");

/**
 Generate device token string for being used in remote notification
 @param deviceToken device token data which will be user for push notifiation
 @return the device token string
 */
+ (NSString *)tokenForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken;

@end
