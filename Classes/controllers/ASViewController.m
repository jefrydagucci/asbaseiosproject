//
//  ASViewController.m
//  ASBaseIOSProject
//
//  Created by Jefry Da Gucci on 2/12/15.
//  Copyright (c) 2015 Appslon http://appslon.com. All rights reserved.
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//  http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.
//

#import "ASViewController.h"

@interface ASViewController ()

@end

@implementation ASViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self addKeyboardbserver];
    navBarPositionDefault   = CGPointMake(160, 42);
    navBarPositionHidden    = CGPointMake(160, -22);
    insetNavBarUnhidden = UIEdgeInsetsMake(44, 0, 0, 0);
    insetNavBarHidden   = UIEdgeInsetsMake(20, 0, 0, 0);
    [self configureTheme];
    
    self.automaticallyAdjustsScrollViewInsets = NO;
}

- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    self.title = @"";
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
    self.title = self.navTitle;
    
    [UIApplication sharedApplication].statusBarHidden = NO;
    self.automaticallyAdjustsScrollViewInsets = NO;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
    NSURLCache * const urlCache = [NSURLCache sharedURLCache];
    const NSUInteger memoryCapacity = urlCache.memoryCapacity;
    urlCache.memoryCapacity = 0;
    urlCache.memoryCapacity = memoryCapacity;
    [[NSURLCache sharedURLCache] removeAllCachedResponses];
}

#pragma mark - view
- (void)configureTheme{
}

- (void)hideNavBar:(BOOL)isHidden{
    CALayer *layer = self.navigationController.navigationBar.layer;
    if(isHidden){
        layer.position = navBarPositionHidden;
    }
    else{
        layer.position = navBarPositionDefault;
    }
}

- (void)barButtonBackTapped{
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - UITextFieldDelegate
- (void)textFieldDidBeginEditing:(UITextField *)textField{
    self.txFieldCurrent = textField;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    return [textField endEditing:YES];
}

- (void)textFieldDidEndEditing:(UITextField *)textField{
    self.txFieldCurrent = nil;
}

#pragma mark - UIKeyboard observer
- (void)addKeyboardbserver{
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShow:) name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardDidShow:) name:UIKeyboardDidShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillHide:) name:UIKeyboardWillHideNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardDidHide:) name:UIKeyboardDidHideNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardDidChangeFrame:) name:UIKeyboardDidChangeFrameNotification object:nil];
}
- (void)removeKeyboardbserver{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardDidShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillHideNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardDidHideNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardDidChangeFrameNotification object:nil];
}

#pragma mark - keyboard action
- (void)keyboardWillShow:(NSNotification *)notification{
    NSLog(@"keyboard will show");
    NSDictionary *info = [notification userInfo];
    
    NSValue *aValue = [info objectForKey:UIKeyboardFrameEndUserInfoKey];
    CGRect keyboardRect = [self.view convertRect:[aValue CGRectValue] fromView:nil];
    [self keyboardWillShowWithRect:keyboardRect];
}

- (void)keyboardWillHide:(NSNotification *)notification{
    NSLog(@"keyboard will hide");
    NSDictionary *info = [notification userInfo];
    
    NSValue *aValue = [info objectForKey:UIKeyboardFrameEndUserInfoKey];
    CGRect keyboardRect = [self.view convertRect:[aValue CGRectValue] fromView:nil];
    [self keyboardWillHideWithRect:keyboardRect];
}

- (void)keyboardDidShow:(NSNotification *)notification{
    NSLog(@"keyboard did show");
    NSDictionary *info = [notification userInfo];
    
    NSValue *aValue = [info objectForKey:UIKeyboardFrameEndUserInfoKey];
    CGRect keyboardRect = [self.view convertRect:[aValue CGRectValue] fromView:nil];
    [self keyboardDidShowWithRect:keyboardRect];
}

- (void)keyboardDidHide:(NSNotification *)notification{
    NSLog(@"keyboard did hide");
    NSDictionary *info = [notification userInfo];
    
    NSValue *aValue = [info objectForKey:UIKeyboardFrameEndUserInfoKey];
    CGRect keyboardRect = [self.view convertRect:[aValue CGRectValue] fromView:nil];
    [self keyboardDidHideWithRect:keyboardRect];
}

- (void)keyboardDidChangeFrame:(NSNotification *)notification{
    NSLog(@"keyboard did change frame");
    NSDictionary *info = [notification userInfo];
    
    NSValue *aValue = [info objectForKey:UIKeyboardFrameEndUserInfoKey];
    CGRect keyboardRect = [self.view convertRect:[aValue CGRectValue] fromView:nil];
    [self keyboardDidChangeFrameWithRect:keyboardRect];
}

- (void)keyboardWillShowWithRect:(CGRect)keyboardRect{
}

- (void)keyboardWillHideWithRect:(CGRect)keyboardRect{
}

- (void)keyboardDidShowWithRect:(CGRect)keyboardRect{
    
}

- (void)keyboardDidHideWithRect:(CGRect)keyboardRect{
    
}

- (void)keyboardDidChangeFrameWithRect:(CGRect)keyboardRect{
    
}

@end