//
//  ASViewController.h
//  ASBaseIOSProject
//
//  Created by Jefry Da Gucci on 2/12/15.
//  Copyright (c) 2015 Appslon http://appslon.com. All rights reserved.
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//  http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.
//

#import <UIKit/UIKit.h>

@interface ASViewController : UIViewController
<UITextFieldDelegate>

{
@protected
    CGFloat scrollerOffsetY;
    CGPoint navBarPositionDefault;
    CGPoint navBarPositionHidden;
    UIEdgeInsets insetNavBarUnhidden;
    UIEdgeInsets insetNavBarHidden;
}


@property (strong, nonatomic) UITextField *txFieldCurrent;
@property (strong, nonatomic) NSString *navTitle;

- (void)configureTheme;

/**
 Method to hide navigation bar
 @param isHidden Value for navigation bar hidden. Set to true to hide navigation bar
 */
- (void)hideNavBar:(BOOL)isHidden;

#pragma mark - UIKeyboard observer
- (void)addKeyboardbserver;
- (void)removeKeyboardbserver;

#pragma mark - keyboard action
- (void)keyboardWillShow:(NSNotification *)notification;
- (void)keyboardWillHide:(NSNotification *)notification;
- (void)keyboardDidShow:(NSNotification *)notification;
- (void)keyboardDidHide:(NSNotification *)notification;
- (void)keyboardDidChangeFrame:(NSNotification *)notification;
- (void)keyboardWillShowWithRect:(CGRect)keyboardRect;
- (void)keyboardWillHideWithRect:(CGRect)keyboardRect;
- (void)keyboardDidShowWithRect:(CGRect)keyboardRect;
- (void)keyboardDidHideWithRect:(CGRect)keyboardRect;
- (void)keyboardDidChangeFrameWithRect:(CGRect)keyboardRect;

@end
