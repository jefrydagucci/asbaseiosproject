//
//  ASTableViewController.h
//  ASBaseIOSProject
//
//  Created by Jefry Da Gucci on 2/16/15.
//  Copyright (c) 2015 Appslon http://appslon.com. All rights reserved.
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//  http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.
//
//

typedef enum {
    RefreshContentTypeAddToLastIndex    = 0,
    RefreshContentTypeAddToFirstIndex   = 1,
    RefreshContentTypeReplaceAll        = 2
}RefreshContentType;

#import "ASViewController.h"

@interface ASTableViewController : ASViewController
<UITableViewDataSource, UITableViewDelegate, UIScrollViewDelegate>
{
    
@protected
    CGFloat bottomOffsetReloadContent;

}

@property (strong, nonatomic) IBOutlet UITableView *tableView;

@property (nonatomic, assign) CGFloat minimumValYOffsetReloadContent;
@property (nonatomic, assign) BOOL isRequestData;
@property (nonatomic, assign) BOOL hasDoneFirstRequest;
@property (nonatomic, assign) CGFloat valVerticalTableMovement;

@property (nonatomic, strong) NSIndexPath *currentIndexPath;
#pragma mark - ASTableViewController method
- (void)ASTableViewReloadContentByPullingTopView:(UITableView *)tableView;
- (void)ASTableViewReachBottom:(UITableView *)scrollView;
- (void)ASTableViewViewReachTop:(UITableView *)scrollView;

- (void)setScrollView:(UIScrollView *)scrollView isScrollNavigationBar:(BOOL)isScrollNavigation;

- (void)ASTableViewControllerNavbarScroller:(UIScrollView *)scrollView scrollingNavigationBar:(UINavigationBar *)navigationBar;
- (void)ASTableViewControllerNavbarScroller:(UIScrollView *)scrollView navigationBarHidden:(BOOL)hidden duration:(NSTimeInterval)duration;

@end
