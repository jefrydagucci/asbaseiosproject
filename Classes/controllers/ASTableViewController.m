//
//  ASTableViewController.m
//  ASBaseIOSProject
//
//  Created by Jefry Da Gucci on 2/16/15.
//  Copyright (c) 2015 Appslon http://appslon.com. All rights reserved.
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//  http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.
//

#import "ASTableViewController.h"

@interface ASTableViewController ()
{
    CGFloat savedContentOffsetY;
}

@property (nonatomic) BOOL isDecelerate;
@property (nonatomic, strong) NSMutableArray *navBarScrollers;

@end

@implementation ASTableViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    bottomOffsetReloadContent = 200;
    self.minimumValYOffsetReloadContent = -70.0f;
    self.automaticallyAdjustsScrollViewInsets = NO;
    self.tableView.contentInset = insetNavBarUnhidden;
    savedContentOffsetY = self.tableView.contentInset.top * -1;
    
    self.navBarScrollers = [NSMutableArray new];
}

- (void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    self.tableView.contentInset = insetNavBarUnhidden;
    self.tableView.dataSource   = self;
    self.tableView.delegate     = self;
    self.tableView.contentOffset = CGPointMake(0, savedContentOffsetY);
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    self.tableView.dataSource   = self;
    self.tableView.delegate     = self;
    self.automaticallyAdjustsScrollViewInsets = NO;
    self.tableView.contentInset = insetNavBarUnhidden;
    [self performSelector:@selector(updateContentOffset) withObject:nil afterDelay:0.0];
}

- (void)updateContentOffset{
    self.tableView.contentOffset = CGPointMake(0, savedContentOffsetY);
}

- (void)viewDidDisappear:(BOOL)animated{
    self.tableView.dataSource   = nil;
    self.tableView.delegate     = nil;
    [super viewDidDisappear:animated];
    savedContentOffsetY = self.tableView.contentOffset.y;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - UITableViewDataSource
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return 0;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    return [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"identifier"];
}

#pragma mark - UIScrollViewDelegate
- (void)scrollViewDidScroll:(UIScrollView *)scrollView{
    
    // table view
    if(scrollView == self.tableView){
        CGPoint scrollOffset    = scrollView.contentOffset;
        CGSize scrollSize       = scrollView.contentSize;
        CGRect scrollFrame      = scrollView.frame;
        if(scrollOffset.y >= scrollSize.height - scrollFrame.size.height&&
           scrollSize.height >= scrollFrame.size.height){
            [self ASTableViewReachBottom:(UITableView *)scrollView];
        }
        else if(scrollOffset.y == 0){
            [self ASTableViewViewReachTop:(UITableView *)scrollView];
        }
    }
    
    //nav bar scrollers
    if([self.navBarScrollers containsObject:scrollView]){
        
        CGFloat contentOffsetY = scrollView.contentOffset.y;
        CGSize scrollSize = scrollView.contentSize;
        CGPoint scrollOffset = scrollView.contentOffset;
        CGRect scrollFrame = scrollView.frame;
        
        if(contentOffsetY == scrollerOffsetY ||
           _isDecelerate||
           (scrollSize.height - scrollOffset.y <= scrollFrame.size.height)){
            return;
        }
        
        if(((contentOffsetY > scrollerOffsetY &&
             contentOffsetY>0))){
            NSTimeInterval duration = 0.3;
            [UIView animateWithDuration:duration animations:^{
                [self hideNavBar:YES];
                [self ASTableViewControllerNavbarScroller:scrollView navigationBarHidden:YES duration:duration];
            }completion:nil];
        }
        else if(scrollSize.height - scrollOffset.y == scrollFrame.size.height){
            scrollOffset.y = scrollSize.height - scrollFrame.size.height + 64;
            scrollView.contentOffset = scrollOffset;
        }
        else{
            NSTimeInterval duration = 0.3;
            [UIView animateWithDuration:duration animations:^{
                [self hideNavBar:NO];
                [self ASTableViewControllerNavbarScroller:scrollView navigationBarHidden:NO duration:duration];
            }completion:nil];
        }
        scrollerOffsetY = scrollOffset.y;
        [self ASTableViewControllerNavbarScroller:scrollView scrollingNavigationBar:self.navigationController.navigationBar];
    }
}

- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate{
    if(scrollView==self.tableView){
        CGPoint offset = scrollView.contentOffset;
        if(offset.y <= self.minimumValYOffsetReloadContent){
            [self ASTableViewReloadContentByPullingTopView:(UITableView *)scrollView];
        }
    }
    
    if(decelerate){
        _isDecelerate = decelerate;
    }
}

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView{
    _isDecelerate = NO;
}

- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView{
    _isDecelerate = NO;
}

#pragma mark - ASBaseTableViewController scroller
- (void)ASTableViewReloadContentByPullingTopView:(UITableView *)tableView{
    NSLog(@"reload content by pulling top view");
}

- (void)ASTableViewReachBottom:(UITableView *)scrollView{
    NSLog(@"fire ASTableViewReachBottom");
}

- (void)ASTableViewViewReachTop:(UITableView *)scrollView{
    NSLog(@"fire ASTableViewViewReachTop");
}

- (void)setScrollView:(UIScrollView *)scrollView isScrollNavigationBar:(BOOL)isScrollNavigation{
    isScrollNavigation?[self.navBarScrollers addObject:scrollView]:[self.navBarScrollers removeObject:scrollView];
    scrollView.delegate = self;
    scrollerOffsetY = (isScrollNavigation?scrollView.contentOffset.y:0);
}

- (void)ASTableViewControllerNavbarScroller:(UIScrollView *)scrollView scrollingNavigationBar:(UINavigationBar *)navigationBar{
}

- (void)ASTableViewControllerNavbarScroller:(UIScrollView *)scrollView navigationBarHidden:(BOOL)hidden duration:(NSTimeInterval)duration{
    scrollView.contentInset = (hidden?insetNavBarHidden:insetNavBarUnhidden);
}

@end
