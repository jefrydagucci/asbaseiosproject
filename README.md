# README #

ASBaseIOSProject is a collection of codes which is often used in ios application development. This include some classes to help developer in developing iOS application such as database manager, document manager, remote notification manager, controllers, some helpers code and so on.
You may check the documentation for more details.

## Requirements
* iOS 7.0 or later

## Installation

ASBaseIOSProject is available through [CocoaPods](http://cocoapods.org), to install
it simply add the following line to your Podfile:
```
pod 'ASBaseIOSProject'
```
After installation finished, you may import the header into your project:
```
#import <ASBaseIOSProject/ASBaseIOSProject.h>
```

## ASDatabaseManager
This is an example how to set the database for your project:
```
[[ASDatabaseManager sharedInstance] setDBName:@"Database name"];
```

## Example using of ASRemoteNotificationManager

This is an example to get device token string from the device token data:
```
- (void)application:(UIApplication *)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken{
    
    NSString *token = [ASRemoteNotificationManager tokenForRemoteNotificationsWithDeviceToken:deviceToken];
}
```

This is an example how to register remote notification in iOS version below 8.0:
```
- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    [ASRemoteNotificationManager registerRemoteNotificationWithRemoteNotificationTypes:UIRemoteNotificationTypeAlert|UIRemoteNotificationTypeBadge|UIRemoteNotificationTypeBadge];
    
    return YES;
}
```

This is an example how to register remote notification in iOS 8.0 above:
```
- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    [ASRemoteNotificationManager registerRemoteNotificationWithUserNotificationTypes:UIUserNotificationTypeAlert|UIUserNotificationTypeBadge];
    
    return YES;
}
```